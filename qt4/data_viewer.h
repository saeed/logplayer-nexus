#ifndef DATA_VIEWER_H
#define DATA_VIEWER_H

#include <QtCore>
#include <QtXml>
#include <QObject>

#include <string>
#include <map>
using namespace std;

namespace tempux
{

class Object;
class DataSet;

class BasicObject//:public QObject
{
    //Q_OBJECT
public:
    BasicObject(const string & s);
    DataSet * parent;
    string name;
    int id;
    bool showCheck ;
    virtual string title() const =0;
};

class DataSet : public BasicObject
{
public:
    DataSet( const string & s ) ;
    vector <Object*> mObjects;
    vector <DataSet*> mDataSets;
    void addObject ( Object * o )
    {
        mObjects.push_back(o);
    }
    void addDataSet ( DataSet * ds )
    {
        mDataSets.push_back(ds);
    }
    string title() const;


};


class Object:public BasicObject
{
public:
    Object ( const string & n);
    string type;
    string color;
    map <string,string> M;
    string title() const;
};

}



class DataViewer
{
public:
    DataViewer();

    static DataViewer & instance();

    int currentCycle ;

    int counter ;

    QVector<tempux::BasicObject*> dataList;

    tempux::DataSet * currentCycleDataSet ;

    void parseCurrentCycle();

    void parseAttributes (tempux::Object * o , const QDomElement &e )
    {
        for ( int i=0;i<e.attributes().count();++i )
        {
            QDomAttr att = e.attributes().item(i).toAttr();
            o->M[att.name().toStdString()]=att.value().toStdString();
        }
    }

    void parseObjects (tempux::DataSet * ds , const QDomElement &e)
    {
        for ( QDomElement objectElement = e.firstChildElement("object") ;
              !objectElement.isNull() ;
              objectElement=objectElement.nextSiblingElement("object") )
        {
            tempux::Object * o = new tempux::Object(objectElement.attribute("name").toStdString());
            QDomElement typeElement =  objectElement.firstChildElement("type");
            o->type=typeElement.attribute("name").toStdString();
            parseAttributes(o,typeElement);
            QDomElement colorElement = objectElement.firstChildElement("color");
            o->color=colorElement.attribute("name").toStdString();
            ds->addObject(o);
            o->parent=ds;
            o->id=counter++;
            dataList.push_back(o);
        }
    }

    void parseDataSets(tempux::DataSet * ds,const QDomElement &e)
    {
        for ( QDomElement dataSetElement = e.firstChildElement("dataSet") ;
              !dataSetElement.isNull() ;
              dataSetElement=dataSetElement.nextSiblingElement("dataSet") )
        {
            tempux::DataSet * dataSet = parseDataSet(dataSetElement);
            ds->addDataSet(dataSet);
            dataSet->parent=ds;
            dataSet->id=counter++;
            dataList.push_back(dataSet);
        }
    }

    tempux::DataSet* parseDataSet (const QDomElement &e)
    {
        tempux::DataSet * ds = new tempux::DataSet(e.attribute("name").toStdString());
        parseObjects(ds,e);
        parseDataSets(ds,e);
        return ds;
    }





};

#endif // DATA_VIEWER_H
