#include "data_viewer.h"



DataViewer &
DataViewer::instance()
{
    static DataViewer s_instance;
    return s_instance;
}

DataViewer::DataViewer()
{
    currentCycle=-1;
    counter = 0;
}

void DataViewer::parseCurrentCycle()
{
    QDomDocument doc("mydocument");
    QFile file(QString("/home/mohsen/.data_viewer/c"+QString::number(currentCycle)+".xml"));
    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug("cant open file");
        return ;
    }
    if (!doc.setContent(&file)) {
        qDebug("cant set contet");
        file.close();
        return ;
    }
    file.close();
  //  qDebug("doc loaded okay");


    QDomElement root = doc.namedItem("cycle").toElement();
    if ( root.isNull() )
    {
        qDebug("cant load root");
    }
    dataList.clear();
    counter=0;
    currentCycleDataSet = parseDataSet(root);
    currentCycleDataSet->name="cycle";
    currentCycleDataSet->id=counter++;
    currentCycleDataSet->parent=0;
    dataList.push_back(currentCycleDataSet);
    //parseDataSets(currentCycleDataSet,root);
    //qDebug()<<"COUNTER"<<counter;
}


string tempux::Object::title() const
{
    return (parent->title()+","+name);
}

string tempux::DataSet::title() const
{
    if (parent!=0)
        return parent->title()+","+name;
    return name;
}

tempux::BasicObject::BasicObject(const string & s):name(s)
{
    parent=0;
    showCheck = true;
}

tempux::DataSet::DataSet( const string & s ):BasicObject(s)
{
}

tempux::Object::Object(const string &n):BasicObject(n)
{
}

