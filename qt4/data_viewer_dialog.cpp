#include "data_viewer_dialog.h"
#include "ui_data_viewer_dialog.h"

#include "data_viewer.h"

DataViewerDialog::DataViewerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DataViewerDialog)
{
    ui->setupUi(this);
    connect(ui->lineEdit,SIGNAL(textChanged(QString)),this,SLOT(updateLabels()));
    connect(ui->checkBox,SIGNAL(clicked()),this,SLOT(checkBoxUpdate()));
    this->resize(320,700);

}

DataViewerDialog::~DataViewerDialog()
{
    delete ui;
}

void DataViewerDialog::showDataID(const int & id)
{
    const DataViewer & dv = DataViewer::instance();
    tempux::BasicObject * o = dv.dataList[id];
    ui->checkBox->setChecked(o->showCheck);
    ui->nameValueLabel->setText(QString(dv.dataList[id]->title().c_str()));
    tempux::Object * obj;
    if ( obj = dynamic_cast<tempux::Object*> (o) )
    {
        for (map<string,string>::iterator it = obj->M.begin();
                it!=obj->M.end();++it)
        {
            ui->gridLayout_3->addWidget(new QLabel(it->first.c_str()),ui->gridLayout_3->rowCount(),0);
            ui->gridLayout_3->addWidget(new QLabel(it->second.c_str()),ui->gridLayout_3->rowCount()-1,1);
        }
    }
    if ( o->parent )
        ui->parentIDValueLabel->setText( QString::number(o->parent->id) );
    else
        ui->parentIDValueLabel->setText("No Parent");

}

void DataViewerDialog::checkBoxUpdate()
{
    const DataViewer & dv = DataViewer::instance();
    int id = ui->lineEdit->text().toInt();
    tempux::BasicObject * o = dv.dataList[id];
    o->showCheck=ui->checkBox->isChecked();
    emit configured();
}


void DataViewerDialog::updateLabels()
{
    while (1)
    {
        QLayoutItem * l = ui->gridLayout_3->takeAt(0);
        if (!l)
            break;
        delete l->widget();
    }

    const DataViewer & dv = DataViewer::instance();
    int requested_id = ui->lineEdit->text().toInt();
    if ( dv.counter==0 )
        return;
    else if ( requested_id < dv.counter && requested_id >= 0 )
    {
        showDataID(requested_id);
    }
    else
        showDataID(dv.counter-1);
}
