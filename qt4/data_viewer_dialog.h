#ifndef DATA_VIEWER_DIALOG_H
#define DATA_VIEWER_DIALOG_H

#include <QDialog>

namespace Ui {
class DataViewerDialog;
}

class DataViewerDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit DataViewerDialog(QWidget *parent = 0);
    ~DataViewerDialog();
    
private:
    Ui::DataViewerDialog *ui;

    void showDataID(const int & id);

public slots:
    void checkBoxUpdate();
    void updateLabels();

signals:
    void configured();

};

#endif // DATA_VIEWER_DIALOG_H
